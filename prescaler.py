from migen import *

"""
PreScaler module

Author: NIIBE Yutaka <gniibe@fsij.org>

This file is a part of Im_GPL, a hardware module for users' libre computing.

Im_GPL is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

For detail, please read accompanied documentation.
"""

class PreScaler(Module):
    def __init__(self,scale):
        self.scale = scale
        self.clk_out = Signal()
        counter = Signal(max=scale)
        counter_max0 = scale//2 - 1;
        counter_max1 = scale - counter_max0 - 2;

        if counter_max0 != counter_max1:
            # print("Warning: PreScaler: duty cycle is not 50-50")
            self.sync += \
                If( counter==0,
                    If( self.clk_out==0,
                        counter.eq(counter_max0)
                    ).Else(
                        counter.eq(counter_max1)
                    ),
                    self.clk_out.eq(~self.clk_out)
                ).Else(
                    counter.eq(counter - 1),
                )
        else:
            self.sync += \
                If( counter==0,
                    counter.eq(scale//2 - 1),
                    self.clk_out.eq(~self.clk_out)
                ).Else(
                    counter.eq(counter - 1),
                )

def test_PreScaler(m,cycles):
    counter = 0
    for cycle in range(cycles):
        yield
        assert (yield m.clk_out) == (1 if counter < m.scale//2 else 0)
        counter = counter + 1
        if counter >= m.scale:
            counter = 0

if __name__ == "__main__":
    import sys
    if len(sys.argv) >= 2 and sys.argv[1] == "sim":
        if len(sys.argv) > 2:
            scale = int(sys.argv[2])
        else:
            scale = 3
        if len(sys.argv) > 3:
            cycles = int(sys.argv[3])
        else:
            cycles = 64
        m = PreScaler(scale)
        run_simulation(m, test_PreScaler(m,cycles),
                       vcd_name="prescaler.vcd")
    elif len(sys.argv) >= 2 and sys.argv[1] == "build":
        # Now, we assume the HX8K board
        from migen.build.generic_platform import *
        from migen.build.platforms import ice40_hx8k_b_evn

        io_ext =  [ ("pin_a4", 37, Pins("T1"),  IOStandard("LVCMOS33")),
                    ("pin_a5", 38, Pins("T2"),  IOStandard("LVCMOS33")) ]

        plat = ice40_hx8k_b_evn.Platform()
        plat.add_extension(io_ext)

        m = Module()
        pin_a4 = plat.request("pin_a4") # A4 440Hz also known as A440
        pin_a5 = plat.request("pin_a5") # A5 880Hz

        m.submodules.a4 = PreScaler(27272)
        m.comb += pin_a4.eq(m.a4.clk_out)
        m.submodules.a5 = PreScaler(13636)
        m.comb += pin_a5.eq(m.a5.clk_out)

        plat.build(m,use_nextpnr=False,build_name="prescaler")
        if len(sys.argv) >= 3 and sys.argv[2] == "-l":
            prog = plat.create_programmer()
            prog.load_bitstream("build/prescaler.bin")
